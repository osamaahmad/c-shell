#include <unistd.h>
#include <string.h>
#include <sys/wait.h>
#include "common.h"
#include "execution.h"
#include "piping.h"


static void execute(struct Args* args1, struct Args* args2);
static void perform_piping(const struct Args* args, size_t split_point);

int try_piping(const struct Args* args)
{
	for (int i = 0; i < args->argc; i++) {
		if (strcmp(args->argv[i], "|") == 0) {
			perform_piping(args, i);
			return 1;
		}
	}

	return 0;
}

void perform_piping(const struct Args* args, size_t split_point)
{
	// TODO perform error checking

	struct Args args1, args2;
	split_args(args, split_point, &args1, &args2);

	int pid = fork();

	if (pid == 0)
		execute(&args1, &args2);
	else
		wait(NULL);
}

static void execute(struct Args* args1, struct Args* args2)
{
	// TODO perform error checking
	// The parent waits for the child, then executes.

	keep_running = 0;

	int pfd[2];
	int error = pipe(pfd);

	int pid = fork();

	if (pid == 0) {
		close(pfd[0]);
		dup2(pfd[1], STDOUT_FILENO);
		handle_input(args1);
	} else {
		close(pfd[1]);
		dup2(pfd[0], STDIN_FILENO);
		wait(NULL);
		handle_input(args2);
	}
}
