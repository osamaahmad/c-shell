#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include "history.h"
#include "common.h"
#include "internal_commands.h"

struct InternalCommand
{
	const char* str;
	void (*func)(const struct Args* args);
};


struct InternalCommand internal_commands[] = 
{
	{ "cd"              , __cd__         },
	{ "exit"            , __exit__       },
	{ no_history_command, __no_history__ },
};


int try_execute_internal_command(const struct Args* args)
{
	static int n = sizeof(internal_commands) / sizeof(struct InternalCommand);

	// If you wanna complicate thins, you can use a trie for the matching, will be slower tho :D
	for (int i = 0; i < n; i++) {
		if (strcmp(args->argv[0], internal_commands[i].str) == 0) {
			internal_commands[i].func(args);
			return 1;
		}
	}

	return 0;
}

void __cd__(const struct Args* args)
{
	// No path specified
	if (args->argc == 1)
		chdir("/");
	chdir(args->argv[1]);	
}

void __exit__(const struct Args* args)
{
	exit(0);
}

void __no_history__(const struct Args* args)
{
	fprintf(stderr, "There is no previous command.\n");
}
