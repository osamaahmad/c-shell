#include <string.h>
#include "history.h"
#include "common.h"

#include <stdio.h>


char** substitute_arrays(char const* const* a1, size_t n1, size_t i1, char const* const* a2, size_t n2)
{
	size_t n = n1 + n2 - 1;  // -1 because were replaceing one of the items.
	char** a = (char**)malloc(sizeof(char*) * n);

	int i = 0;
	for (int j = 0; j < i1; j++)
		a[i++] = strdup(a1[j]);
	for (int j = 0; j < n2; j++)
		a[i++] = strdup(a2[j]);
	for (int j = i1+1; j < n1; j++)
		a[i++] = strdup(a1[j]);

	return a;
}

struct Args substitute_history(const struct Args* args)
{
	// TODO Implement all "Event Designators"
	//  Reference: http://ftp.gnu.org/old-gnu/Manuals/bash/html_chapter/bashref_9.html#SEC115
	
	struct Args result = duplicate_args(args);

	for (int i = 0; i < args->argc; i++) {
		if (strcmp(args->argv[i], "!!") == 0) {
			struct Args temp = result;
			result.argv = substitute_arrays(result.argv, result.argc, i, last_command.argv, last_command.argc);
			result.argc += last_command.argc - 1;
			free_args(&temp);
		}
	}

	return result;
}
