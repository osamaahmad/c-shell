#ifndef REDIRECTION_H_
#define REDIRECTION_H_

#include "common.h"

int redirected;
int fd_backup;
int last_redirected_fd;

void try_redirect(struct Args* args);
void restore_fd();

#endif
