#ifndef __INTERNAL_COMMANDS_
#define __INTERNAL_COMMANDS_

#include "common.h"

int try_execute_internal_command(const struct Args* args);

void __cd__(const struct Args* args);
void __exit__(const struct Args* args);
void __no_history__(const struct Args* args);


#endif
