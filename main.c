#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "parsing.h"
#include "history.h"
#include "execution.h"
#include "redirection.h"

void init()
{
	input_buff = (char*)malloc(sizeof(char) * MAX_COMMAND_LEN);

	execluded_delimiters = " ";
	included_delimiters = "&<>|";

	// Allocating for both the array pointer and the
	//  command pointer because 2 free operations will
	//  happen, one for the array pointer, and one for
	//  its items.
	last_command.argv = (char**)malloc(sizeof(char*));
	last_command.argv[0] = (char*)strdup(no_history_command);
	last_command.argc = 1;

	redirected = 0;
	keep_running = 1;
}

int main()
{
	init();

	while (keep_running) 
	{
		printf("Shell> ");
		fflush(stdout);

		struct Args args = get_input();
		handle_input(&args);
	}
}
