#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>
#include <string.h>
#include "parsing.h"
#include "history.h"
#include "internal_commands.h"
#include "redirection.h"
#include "piping.h"


static void execute(struct Args* args)
{
	int pid = fork();
	
	if (pid == 0) {
		// Child
		keep_running = 0;
		execvp(args->argv[0], args->argv);
	} else {
		// Parent
		wait(NULL);
	}
}

void handle_input(struct Args* args)
{
	struct Args temp = *args;
	*args = substitute_history(&temp);
	free_args(&temp);

	// PIPING HAPPENS FIRST BEFORE REDIRECTION

	if (!try_piping(args) && !try_execute_internal_command(args))
		execute(args);

	try_redirect(args);

	restore_fd();

	free_args(&last_command);
	last_command = *args;
}
