#include <string.h>
#include "common.h"


void free_ptr_array(char** array, size_t n)
{
	for (int i = 0; i < n; i++)
		free(array[i]);
	free(array);
}

void free_args(struct Args* args)
{
	free_ptr_array(args->argv, args->argc);
}

char* create_char_string(char c)
{
	char* result = (char*)malloc(sizeof(char) * 2);
	result[0] = c;
	result[1] = '\0';
	return result;
}

struct Args duplicate_args(const struct Args* args)
{
	size_t argc = args->argc;
	char** argv = (char**)malloc(sizeof(char*) * argc);
	for (int i = 0; i < argc; i++)
		argv[i] = strdup(args->argv[i]);
	struct Args result = { .argv = argv, .argc = argc };
	return result;
}

void split_args(const struct Args* args, size_t split_point, struct Args* a1, struct Args* a2)
{
	a1->argc = split_point;
	a1->argv = (char**)malloc(sizeof(char*) * a1->argc);
	for (int i = 0; i < a1->argc; i++)
		a1->argv[i] = strdup(args->argv[i]);

	a2->argc = args->argc - (split_point + 1);
	a2->argv = (char**)malloc(sizeof(char*) * a2->argc);
	for (int i = 0, j = split_point + 1; i < a2->argc; i++, j++)
		a2->argv[i] = strdup(args->argv[j]);
}
