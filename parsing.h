#ifndef PARSING_H_
#define PARSING_H_

#include <stdlib.h>
#include "common.h"


#define TEMP_BUFF_LEN 4096
#define MAX_ARGC 128
#define MAX_COMMAND_LEN 4096

#define INCLUDED_DELIMITER 1
#define EXECLUDED_DELIMITER -1
#define INCLUDED_CHAR 0  // DO NOT CHANGE THIS


int lookup_table[256];
char* execluded_delimiters;
char* included_delimiters;
int included_delimiters_count;
int execluded_delimiters_count;
char* input_buff;

int char_type(char c);

// Should free the memory after usage.
struct Args parse_input(char* input, int len);
struct Args get_input();


// TESTING
void test_parsing(char* str);

#endif
