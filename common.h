#ifndef _COMMON_H_
#define _COMMON_H_

#include <stddef.h>
#include <stdlib.h>

struct Args
{
	int argc;
	char** argv;
};

int keep_running;


void free_ptr_array(char** array, size_t n);
void free_args(struct Args* args);

char* create_char_string(char c);
struct Args duplicate_args(const struct Args* args);
void split_args(const struct Args* args, size_t split_point, struct Args* a1, struct Args* a2);

#endif
