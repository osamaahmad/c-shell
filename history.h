#ifndef _HISTORY_H_
#define _HISTORY_H_

#include "common.h"

#define no_history_command "__NoLastCommand__"

struct Args last_command;

// Replace a1[i1] with the content of a2.
char** substitute_arrays(char const* const* a1, size_t n1, size_t i1, char const* const* a2, size_t n2);

struct Args substitute_history(const struct Args* args);

#endif
