#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include "redirection.h"

void redirection_common(struct Args* args, int i, int newfd)
{
	// TODO This way, everything after < and > are
	//  discarded, this is not a convinient thing to have.
	
	redirected = 1;
	fd_backup = dup(newfd);
	last_redirected_fd = newfd;

	int oldfd = open(args->argv[i+1], O_CREAT | O_RDWR, 0666);
	dup2(oldfd, newfd);
	close(oldfd);

	for (int j = i; j < args->argc; j++)
		free(args->argv[j]);

	args->argv[i] = NULL;
	args->argc = i - 1;
}

void try_redirect(struct Args* args)
{
	// TODO perfoem some error checking.

	for (int i = 0; i < args->argc; i++)
	{
		// command > file
		if (strcmp(args->argv[i], ">") == 0)
			return redirection_common(args, i, STDOUT_FILENO);
		
		// commadn < file
		if (strcmp(args->argv[i], "<") == 0)
			return redirection_common(args, i, STDIN_FILENO);
	}
}

void restore_fd()
{
	if (!redirected) return;

	dup2(fd_backup, last_redirected_fd);
	close(fd_backup);
	redirected = 0;
}
