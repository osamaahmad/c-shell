#include <stdio.h>
#include <string.h>
#include "parsing.h"


int char_type(char c)
{
	static int is_initialized = 0;

	if (!is_initialized) {
		execluded_delimiters_count = strlen(execluded_delimiters);
		included_delimiters_count = strlen(included_delimiters);
		is_initialized = 1;
		memset(lookup_table, INCLUDED_CHAR, sizeof(lookup_table));
		for (int i = 0; i < execluded_delimiters_count; i++) {
			lookup_table[execluded_delimiters[i]] = EXECLUDED_DELIMITER;
		}
		for (int i = 0; i < included_delimiters_count; i++) {
			lookup_table[included_delimiters[i]] = INCLUDED_DELIMITER;
		}
	}
			
	return lookup_table[c];
}

struct Args parse_input(char* input, int len)
{
	struct Args args;
	args.argv = (char**)malloc(sizeof(char*) * MAX_ARGC);
	args.argc = 0;
	int temp_index = 0;
	char temp_buff[TEMP_BUFF_LEN];
	
	for (int i = 0; i < len; i++) 
	{
		int type = char_type(input[i]);
		if (type != INCLUDED_CHAR) 
		{
			if (temp_index != 0) {
				args.argv[args.argc++] = strndup(temp_buff, temp_index);
				temp_index = 0;
			}

			if (type == INCLUDED_DELIMITER) {
				args.argv[args.argc++] = create_char_string(input[i]);
			}

			continue;
		}

		temp_buff[temp_index++] = input[i];
	}
	
	if (temp_index != 0) {
		args.argv[args.argc++] = strndup(temp_buff, temp_index);
	}

	return args;
}

struct Args get_input()
{
	scanf("%[^\n]%*c", input_buff); // Read a line
	return parse_input(input_buff, strlen(input_buff));
}

void test_parsing(char* str)
{
	printf("String = %s\n", str);
	printf("Parsing:\n");

	struct Args args = parse_input(str, strlen(str));

	for (int i = 0; i < args.argc; i++) {
		printf("\t\"%s\"\n", args.argv[i]);
	}

	printf("Argc: %d\n", args.argc);
}
